package pl.bk.advanced_programming;


@FunctionalInterface
interface  SayHello{
    void sayHallo();

    default void goodbye() {
        System.out.println("Goodbye!!");

    }
}

@FunctionalInterface
interface Test {
    void testMethod (int a, String name);
    default void defaultTestMethod(int a){
        System.out.println(a);
    }
    default String nextTest (String name){
        return name;
    }

}
public class HellowLambda{
    public static void main(String[] args) {
        SayHello hello = () -> {};
        hello.sayHallo();

        useTest((a,b)-> System.out.println("" + a + a +a+ "----" + b + b + b)
        ,12,"dodo");
        useTest(new Test() {
            @Override
            public void testMethod(int a, String name) {
                System.out.println("a = "+a+"; name = "+ name);
            }
        },5,"a");
    }
    static void useTest (Test test, int a, String name){
        test.testMethod(a, name);
        test.defaultTestMethod(a);
        System.out.println(test.nextTest(name));
    }
}
