package pl.bk.advanced_programming;

public class ImplementInterfaceFromLambda implements Test {
    @Override
    public void testMethod(int a, String name) {

    }

    @Override
    public void defaultTestMethod(int a) {

    }

    @Override
    public String nextTest(String name) {
        return null;
    }
}
