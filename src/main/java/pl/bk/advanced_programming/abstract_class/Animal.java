package pl.bk.advanced_programming.abstract_class;

import java.util.Objects;

public abstract class Animal {

    private static final int NAME_MIN_LEN = 1;
    private String name;

    public Animal(String name) {
        if (name.length() < NAME_MIN_LEN){
            throw new RuntimeException("name top short: [" + name + "], minimal lenghtis " + NAME_MIN_LEN);
        }
        this.name = Objects.requireNonNull(name,"[name] cannot be null!");
    }

    public abstract void eat();

    public abstract void play();

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                '}';
    }
}
