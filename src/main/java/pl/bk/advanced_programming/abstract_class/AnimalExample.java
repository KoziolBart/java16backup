package pl.bk.advanced_programming.abstract_class;

public class AnimalExample {
    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        zoo.handleAnimal(new Animal("Alpaka") {
            @Override
            public void eat() {

                System.out.println("eat Alpaka");
            }

            @Override
            public void play() {
                System.out.println("play Alpaka");

            }
        });
        zoo.handleAnimal(new Animal("Dog") {
            @Override
            public void eat() {
                System.out.println("eat Dog");
            }

            @Override
            public void play() {
                System.out.println("play Dog");

            }
        });
        Animal cat = null;
        zoo.handleAnimal(cat = new Animal("Cat") {
            @Override
            public void eat() {
                System.out.println("eat Cat");
            }

            @Override
            public void play() {
                System.out.println("play Cat");

            }
            public Animal miauu() {
                System.out.println("miauu");
                return new Animal("Cat") {
                    @Override
                    public void eat() {

                    }

                    @Override
                    public void play() {

                    }
                };
            }
        }.miauu());


    }
}
