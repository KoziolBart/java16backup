package pl.bk.advanced_programming.abstract_class;

public class Cat extends Animal{
    public Cat(String name) {
        super(name);
    }

    public static void main(String[] args) {
    }

    @Override
    public void eat() {

    }

    @Override
    public void play() {

    }

    @Override
    public String toString() {
        return "Cat{}" + super.toString();
    }
}
