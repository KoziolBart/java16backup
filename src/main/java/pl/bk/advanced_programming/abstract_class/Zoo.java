package pl.bk.advanced_programming.abstract_class;

public class Zoo {

    public void handleAnimal(Animal animal){
        animal.eat();
        animal.play();
        animal.play();
    }
}
