package pl.bk.advanced_programming.comparing;

import java.time.Year;

public class Car implements Comparable<Car>{
    private String brand;
    private String model;
    private float capasity;
    private String color;
    private Year year;

    public Car(String brand, String model, float capasity, String color, Year localDate) {
        this.brand = brand;
        this.model = model;
        this.capasity = capasity;
        this.color = color;
        this.year = localDate;
    }

    public Car() {
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", capasity=" + capasity +
                ", color='" + color + '\'' +
                ", year=" + year +
                "}\n";
    }

//    public void mayByComparing(Comparator<Car> comparator, List<Car> carList){
//        Collections.sort(carList, comparator);
//        System.out.println(carList);
//    }

//   public int mayByComparing(Comparable<Car> comparable){

//       comparable.compareTo(this);
//   }

    @Override
    public int compareTo(Car o) {
        return this.brand.compareTo(o.brand);
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public float getCapasity() {
        return capasity;
    }

    public String getColor() {
        return color;
    }

    public  Year getYear() {
        return year;
    }
}
