package pl.bk.advanced_programming.comparing;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import pl.bk.advanced_programming.comparing.ref_method.CompareByAllArgument;
import pl.bk.advanced_programming.comparing.ref_method.CompareByModel;

import java.time.LocalDate;
import java.time.Year;
import java.util.*;

public class Example {

    private static void presonTest() {
        List<Person> people = Arrays.asList(
                new Person("Az", "A", 100),
                new Person("By", "B", 99),
                new Person("Cx", "C", 98),
                new Person("Dw", "D", 97),
                new Person("Eu", "E", 96),
                new Person("Fj", "F", 95),
                new Person("Gr", "G", 94),
                new Person("Ht", "H", 193)
        );

//            people.add(new Person("Nowa", "Nowsza", 200));  - nie da się dodwać do listy asList

        System.out.println(people);
        Collections.sort(people);
        System.out.println(people);
        Collections.sort(people, (Comparator.comparingInt(o -> o.getName().charAt(1))));
        System.out.println(people);
        Collections.sort(people, (Comparator.comparing(Person::getSurname).thenComparing(Person::getName)));
        System.out.println(people);
    }

    private static void carTest() {
        List<Car> car = Arrays.asList(
                new Car("Toyota", "Avenssis", 2, "black", Year.now()),
                new Car("Audi", "A3", 1.6f, "blue", Year.of(2011)),
                new Car("BMW", "X6", 3.5f, "white", Year.of(2015)),
                new Car("BMW", "X6", 3.5f, "white", Year.of(2011)),
                new Car("Mini", "One", 1.2f, "green", Year.of(2000)),
                new Car("Fiat", "126p", 1.8f, "red", Year.of(1990)),
                new Car("Fiat", "125p", 1.0f, "red", Year.of(1989))

        );
        Collections.sort(car);
        System.out.println(car);
        Collections.sort(car, Comparator.comparing(Car::getYear).reversed());
        System.out.println(car);
        Collections.sort(car, Comparator.comparing(Car::getModel).thenComparing(Car::getBrand));
        System.out.println(car);

        Collections.sort(car, new CarBrandComparator());
        System.out.println(car);

        car.sort((o1, o2) -> o1.getBrand().compareTo(o2.getBrand())); // sort poniżej wykonuje ta sama metode
        System.out.println(car);

        car.sort(Comparator.comparing(Car::getBrand));
        System.out.println(car);

        CompareByModel compareByModel = new CompareByModel();
        car.sort((o1, o2) -> compareByModel.orderByModel(o1, o2));
        car.sort(compareByModel::orderByModel);

        System.out.println(car);

        car.sort(new CompareByAllArgument()::theBest);
        System.out.println(car);
        //   car.sort(((o1, o2) -> CompareByAllArgument.theBest(o1,o2))); - inne rozwiązanie, ale metoda musi być static


        //  Car car1 = new Car();
        //  car1.mayByComparing(Comparator.comparing(o1 -> o1.getYear()),car);
        car.sort(
                Comparator.comparing(Car::getYear)
                .thenComparing(Car::getModel)
                .thenComparing(Car::getColor)
                .thenComparing(Car::getBrand)
        );
        System.out.println(car);
    }

    public static void main(String[] args) {

        carTest();
    }

}
