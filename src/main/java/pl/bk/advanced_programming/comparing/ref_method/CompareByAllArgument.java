package pl.bk.advanced_programming.comparing.ref_method;

import pl.bk.advanced_programming.comparing.Car;

public class CompareByAllArgument {
    public int theBest(Car o1, Car o2) {
        if(!o1.getBrand().equals(o2.getBrand())){
            return o1.getBrand().compareTo(o2.getBrand());
        } else if(!o1.getModel().equals(o2.getModel())) {
            return o1.getModel().compareTo(o2.getModel());
        } else if(!o1.getColor().equals(o2.getColor())){
            return o1.getColor().compareTo(o2.getColor());
        } else if(o1.getYear().getValue()!=o2.getYear().getValue()){
            return o1.getYear().getValue() - o2.getYear().getValue();
        } else return 0;

    }
}
