package pl.bk.advanced_programming.generic;

import pl.bk.advanced_programming.abstract_class.Animal;
import pl.bk.advanced_programming.abstract_class.Cat;
import pl.bk.advanced_programming.abstract_class.Dog;

public class ArrayTest {
    public static void main(String[] args) {

        Dog[] dogs = new Dog[2];
        Cat[] cats = new Cat[2];
        Animal dog1 = new Dog("asd");
        Animal dog2 = new Dog("fas");
        Animal cat1 = new Cat("sfaf");
        Animal cat2 = new Cat("afsf");
        dogs[0] = (Dog) dog1;
        dogs[1] = (Dog) dog2;
        cats[0] = (Cat) cat1;
        cats[1] = (Cat) cat2;
        Animal[] animals = new Animal[4];
        animals[0] = dog1;
        animals[1] = dog2;
        animals[2] = cat1;
        animals[3] = cat2;
        displayAnimals(animals);
        displayAnimals(cats);
        displayAnimals(dogs);
        addDog(cats);
        addDog(dogs);
        addDog(animals);




    }

    public static void addDog(Animal[] animals){
        animals[0] = new Dog("adsadad");
    }

    public static void displayAnimals(Animal[] animals){
        for (Animal animal: animals){
            System.out.println(animal);
        }
    }
}
