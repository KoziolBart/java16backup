package pl.bk.advanced_programming.generic;

import pl.bk.advanced_programming.abstract_class.Zoo;

import java.math.BigDecimal;

public class ClassWithGenericMethod {
    private String maybyName;
    private Double maybrIncome;
    private BigDecimal hugeNumber;

    public ClassWithGenericMethod(String maybyName, Double maybrIncome, BigDecimal hugeNumber) {
        this.maybyName = validateString(maybyName);
        this.maybrIncome = validateGeneric(maybrIncome);
        this.hugeNumber = validateGeneric(hugeNumber);
    }

    private String validateString(String toCheck){
        if (toCheck == null){
            throw new RuntimeException("Connot by null!!!");
        }
        return toCheck;
    }
    private Double validateDouble(Double toCheck){
        if (toCheck == null){
            throw new RuntimeException("Connot by null!!!");
        }
        return toCheck;
    }
    private <T > T validateGeneric(T toCheck){
        if (toCheck == null){
            throw new RuntimeException("Connot by null!!!");
        }
        return toCheck;

    }
}
