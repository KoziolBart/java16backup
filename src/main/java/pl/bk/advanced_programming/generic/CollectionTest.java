package pl.bk.advanced_programming.generic;

import pl.bk.advanced_programming.abstract_class.Animal;
import pl.bk.advanced_programming.abstract_class.Cat;
import pl.bk.advanced_programming.abstract_class.Dog;

import java.util.ArrayList;
import java.util.List;

public class CollectionTest {
    public static void main(String[] args) {
        Animal dog1 = new Dog("asd");
        Animal dog2 = new Dog("fas");
        Animal cat1 = new Cat("sfaf");
        Animal cat2 = new Cat("afsf");
        List<Dog> dogs = new ArrayList<>();
        dogs.add((Dog) dog1);
        dogs.add((Dog) dog2);
        List<Cat> cats = new ArrayList<>();
        cats.add((Cat) cat1);
        cats.add((Cat) cat2);
        displayAnimal(cats);
        displayAnimal(dogs);
        List<Animal> animals = new ArrayList<>();
        addElementToList(animals);

    }
    public static void displayAnimal(List<?> animals){
        for (Object animal : animals){
            System.out.println(animal);
        }
    }
    public static void addElementToList(List<? super Animal> animals){
        animals.add(new Dog("asdaffaf"));
    }
}
