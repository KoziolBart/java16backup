package pl.bk.advanced_programming.generic;

import pl.bk.advanced_programming.abstract_class.Animal;

public class GenericContainer<T extends Animal,U> {
    private T firstGenericElement;
    private U secondGenericElement;

    public GenericContainer(T firstGenericElement, U secondGenericElement) {
        this.firstGenericElement = firstGenericElement;
        this.secondGenericElement = secondGenericElement;
    }

    public T getFirstGenericElement() {
        firstGenericElement.eat();
        return firstGenericElement;
    }

    public U getSecondGenericElement() {
        return secondGenericElement;
    }

    @Override
    public String toString() {
        return "GenericContainer{" +
                "firstGenericElement=" + firstGenericElement +
                ", secondGenericElement=" + secondGenericElement +
                '}';
    }
}
