package pl.bk.advanced_programming.generic;

public class MySecondGeneric<T> {
    private T element;


    public MySecondGeneric(T element) {
        this.element = element;
    }

    public T getElement() {
        return element;
    }

    @Override
    public String toString() {
        return "MySceondGeneric{" +
                "element=" + element +
                '}';
    }
}
