package pl.bk.advanced_programming.generic;

public class MyStringSecret {
    private String mySecret;

    public MyStringSecret(String mySecret) {
        this.mySecret = mySecret;
    }

    public String getMySecret() {
        return mySecret;
    }

    @Override
    public String toString() {
        return "MyStringSecret{" +
                "mySecret='" + mySecret + '\'' +
                '}';
    }
}
