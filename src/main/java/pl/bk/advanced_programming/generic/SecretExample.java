package pl.bk.advanced_programming.generic;

import java.math.BigDecimal;

public class SecretExample {
    public static void main(String[] args) {
        MyStringSecret deram = new MyStringSecret("hahahah");
        MyBigDecimalSecret incoming = new MyBigDecimalSecret(BigDecimal.TEN);
        MySecretGeneric integer = new MySecretGeneric(Integer.valueOf(1));
        integer.getObject();
        MySecondGeneric<Integer> dreamGeneric = new MySecondGeneric<>(123);
        MySecondGeneric<BigDecimal> bigDecimalGeneric = new MySecondGeneric<>(BigDecimal.ONE);


    }
}
