package pl.bk.advanced_programming.generic.optional;

import java.util.Optional;
import java.util.function.Predicate;

public class GenericOptionalTest {
    public static void main(String[] args) {
        Optional<String> maybyName = Optional.of("Bartosz");
        System.out.println(checkifNameMatch("Bartosz",maybyName));
        System.out.println(checkifNameMatch("Bartoszz",maybyName));
        //System.out.println(checkifNameMatch(null,null)); ERROR

    }
    public static boolean checkifNameMatch(String name, Optional<String> maybeName){
        Predicate predicate = s -> s.equals(name);
        Predicate<String> predicate1 = new Predicate<String>() {
            @Override
            public boolean test(String s) {
               return s.equals(name);
            }
        };
        Predicate<CharSequence> predicate2 = new Predicate<CharSequence>() {
            @Override
            public boolean test(CharSequence charSequence) {
                return name.equals(charSequence);
            }
        };
        System.out.println("Test1: " + maybeName.filter(s -> predicate1.test(s)).isPresent());
        System.out.println("Test2: " + maybeName.filter(predicate2::test).isPresent());
        return maybeName.filter(predicate2).isPresent();

    }
}
