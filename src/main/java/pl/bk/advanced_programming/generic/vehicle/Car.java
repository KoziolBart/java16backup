package pl.bk.advanced_programming.generic.vehicle;

public class Car extends Vehicle{
    private Double power;

    public Car(String name) {
        super(name);

    }

    public Double getPower() {
        return power;
    }

    public void setPower(Double power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Car{" +
                "power=" + power + super.toString() +
                '}';
    }
}
