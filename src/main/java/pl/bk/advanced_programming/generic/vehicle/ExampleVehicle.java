package pl.bk.advanced_programming.generic.vehicle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExampleVehicle {

    public static void main(String[] args) {
        Vehicle v1 = new Car("car1");
        Vehicle v2 = new Car("car2");
        Vehicle v3 = new Car("car3");
        Vehicle v4 = new Plane("plane4");
        Vehicle v5 = new Plane("plane5");
        Vehicle v6 = new Plane("plane6");
        List<Vehicle> vehicles = new ArrayList<>();
        List<Car> cars = new ArrayList<>();
        List<Plane> planes = new ArrayList<>();
        cars.add((Car) v1);
        cars.add((Car) v2);
        cars.add((Car) v3);
        planes.add((Plane) v4);
        planes.add((Plane) v5);
        planes.add((Plane) v6);
        vehicles.addAll(cars);
        vehicles.addAll(planes);
        addNewCarInToList(vehicles);
        addNewCarInToList((cars));
  //      addNewCarInToList((planes));              ERROR
        addNewCarAndVehicleInToList(vehicles);
   //     addNewCarAndVehicleInToList(cars);        ERROR
   //     addNewCarAndVehicleInToList(planes);      ERROR
        System.out.println("Vehicle*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
        displayVehicle(vehicles);
        System.out.println("Car*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        displayVehicle(cars);
        System.out.println("Plane-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
        displayVehicle(planes);
    }
    private static void displayVehicle(List<? extends Vehicle> vehicles){
        vehicles.forEach(System.out::println);
    }
    private static void addNewCarAndVehicleInToList(List<? super Vehicle> vehicles) {
        vehicles.add(new Car("car8"));
        vehicles.add(new Vehicle("vehicle9"));
    }
    private static void addNewCarInToList(List<? super Car> vehicles) {
        vehicles.add(new Car("car7"));
    //    vehicles.add(new Vehicle("vehicle9"));

    }

}

