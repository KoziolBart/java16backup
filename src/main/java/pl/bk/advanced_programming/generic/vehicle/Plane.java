package pl.bk.advanced_programming.generic.vehicle;

public class Plane extends Vehicle{
    private Integer numerEngine;

    public Plane(String name) {
        super(name);
    }

    public Integer getNumerEngine() {
        return numerEngine;
    }

    public void setNumerEngine(Integer numerEngine) {
        this.numerEngine = numerEngine;
    }

    @Override
    public String toString() {
        return "Plane{" +
                "numerEngine=" + numerEngine + super.toString() +
                '}';
    }
}
