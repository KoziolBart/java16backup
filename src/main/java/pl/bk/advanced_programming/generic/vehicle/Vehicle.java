package pl.bk.advanced_programming.generic.vehicle;

public class Vehicle {
    private String name;
    private Integer speed;

    public Double distans(Long time){
        Double l = (double)speed * time;
        return l;

    }

    public Vehicle(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "name='" + name + '\'' +
                ", speed=" + speed +
                '}';
    }
}
