package pl.bk.advanced_programming.lambdaThis;

public class Main {
    private String name = "Main";
    public void move() {
        System.out.println("move() inside main");
    }

    public Moveable makeOne() {
        return new Moveable() {
            private String name = "Moveable";
            @Override
            public void move() {
                System.out.println("inside anonymous class - move()");
                System.out.println(this.name);
                System.out.println(Main.this.name);

            }
        };
    }

    public Moveable makeOneWithLambda() {

        return () -> {
            System.out.println("Lambda");
            System.out.println(this.name);
            this.move();
        };
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {

    }
}
