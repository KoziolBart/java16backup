package pl.bk.advanced_programming.lambdaThis;

@FunctionalInterface
public interface Moveable {
    void move();
}
