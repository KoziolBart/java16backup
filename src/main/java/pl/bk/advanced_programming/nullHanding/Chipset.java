package pl.bk.advanced_programming.nullHanding;

public class Chipset {

    private String name;

    public Chipset(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Chipset{" +
                "name='" + name + '\'' +
                '}';
    }
}
