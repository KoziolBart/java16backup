package pl.bk.advanced_programming.nullHanding;

public class Computer {

    private GraphicsCard graphicsCard;

    public Computer(GraphicsCard graphicsCard) {
        this.graphicsCard = graphicsCard;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "graphicsCard=" + graphicsCard +
                '}';
    }

    public GraphicsCard getGraphicsCard() {
        return graphicsCard;
    }
}
