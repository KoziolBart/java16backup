package pl.bk.advanced_programming.nullHanding;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class Example {

    public static void main(String[] args) {
        List<Computer> computerList = new ArrayList<>();

        Computer fullOne = new Computer(new GraphicsCard(new Chipset("nVidia")));
        Computer withOutVendor = new Computer(new GraphicsCard(new Chipset(null)));
        Computer withOutChipset = new Computer(new GraphicsCard(null));
        Computer withOutGraphicsCard = new Computer(null);
        Computer maybeComputer = null;
        computerList.add(fullOne);
        computerList.add(withOutVendor);
        computerList.add(withOutChipset);
        computerList.add(withOutGraphicsCard);
        computerList.add(maybeComputer);
        computerList.forEach(arg -> System.out.println(getVendorNameSaveImp(arg)));
        computerList.forEach(arg -> System.out.println(getVendorNameJava8Way(arg)));

        Optional<String> maybeName = Optional.ofNullable(null);
        maybeName.ifPresent(s -> System.out.println(s));
        maybeName.ifPresent(System.out::println);






    }
    public static String getVendorNameSuperNaiveimp(Computer computer){
        return computer.getGraphicsCard().getChipset().getName();
    }
    public static String getVendorNameSaveImp(Computer computer){
        if(computer==null){
            return "Computer is null";
        } else if(computer.getGraphicsCard()==null){
            return "GraphicCard is null";
        } else if (computer.getGraphicsCard().getChipset() == null){
            return "Chipset is null";
        } else if (computer.getGraphicsCard().getChipset().getName()==null){
            return "Chipset has not name";
        } else return computer.getGraphicsCard().getChipset().getName();
    }
    public static Optional<String> getVendorNameJava8Way (final Computer computer){
        return Optional.ofNullable(computer)
                .map(computer1 -> computer1.getGraphicsCard())
                .map(graphicsCard -> graphicsCard.getChipset())
                .map(chipset -> chipset.getName());

    }
    public static Optional<String> getVendorNameJava8ShortWay (final Computer computer){
        return Optional.ofNullable(computer)
                .map(Computer::getGraphicsCard)
                .map(GraphicsCard::getChipset)
                .map(Chipset::getName);

    }

}
