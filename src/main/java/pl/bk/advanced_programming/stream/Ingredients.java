package pl.bk.advanced_programming.stream;

public class Ingredients {
    private String name;

    public Ingredients(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
