package pl.bk.advanced_programming.stream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Item {
    private String name;
    private BigDecimal price;
    private List<Ingredients> ingredientsList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public List<Ingredients> getIngredientsList() {
        return ingredientsList;
    }

    public Item(String name, BigDecimal price) {
        this.name = name;
        this.price = price;

    }

    protected void addIngredientToList (Ingredients ingredients) {
        ingredientsList.add(ingredients);
    }
}
