package pl.bk.advanced_programming.stream;

import java.util.ArrayList;
import java.util.List;

public class Order {
    List<Item> myItems = new ArrayList<>();

    public List<Item> getMyItems() {
        return myItems;
    }

    void addItemToList (Item item){
        myItems.add(item);
    }
}

