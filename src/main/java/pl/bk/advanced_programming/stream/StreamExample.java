package pl.bk.advanced_programming.stream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class StreamExample {
    private Order createOrder1(){
        Order order = new Order();
        Item salad = new Item("Salad", BigDecimal.valueOf(15.55));
        salad.addIngredientToList(new Ingredients("salad"));
        salad.addIngredientToList(new Ingredients("tomato"));
        order.addItemToList(salad);
        Item cinema = new Item("Cinema", BigDecimal.valueOf(35.99));
        cinema.addIngredientToList(new Ingredients("paper"));
        order.addItemToList(cinema);
        Item cola = new Item("Cola", BigDecimal.valueOf(5.25));
        cola.addIngredientToList(new Ingredients("carbondioxygen"));
        cola.addIngredientToList(new Ingredients("sugar"));
        order.addItemToList(cola);
        return order;
    }
    private Order createOrder2() {
        Order order = new Order();
        Item bigMac = new Item("BigMac", BigDecimal.valueOf(10.99));
        bigMac.addIngredientToList(new Ingredients("bread"));
        bigMac.addIngredientToList(new Ingredients("cheese"));
        bigMac.addIngredientToList(new Ingredients("meat"));
        order.addItemToList(bigMac);
        Item chips = new Item("Chips", BigDecimal.valueOf(4.54));
        chips.addIngredientToList(new Ingredients("potatoe"));
        order.addItemToList(chips);
        return order;
    }

    private List<String> getIngredient(List<Order> list){
        return list.stream()
                .flatMap(order -> order.getMyItems().stream())
                .flatMap(item -> item.getIngredientsList().stream())
                .map(Ingredients::getName)
                .collect(Collectors.toList());
    }
    private List<String> getIngredientByReduce(List<Order> list){

       return list.stream()
                .flatMap(order -> order.getMyItems().stream())
                .flatMap(item -> item.getIngredientsList().stream())
                .map(Ingredients::getName)
                .map(o1 -> listWithOneElement(o1))
                .reduce(new ArrayList<String>(),(o1,o2) -> collectList(o1,o2));
    }
    private List<String> getIngredientByReduceClassesVersion(List<Order> list){
        List<String> result = new ArrayList<>();
       return list.stream()
                .flatMap(order -> order.getMyItems().stream())
                .flatMap(item -> item.getIngredientsList().stream())
                .map(Ingredients::getName)
                .reduce(result, ((strings, s) -> {
                    strings.add(s);
                    System.out.println(strings);
                    return strings;
                }),((strings, strings2) -> strings));
    }
    private List<String> getIngredientByReduceWithOutNewMethod(List<Order> list){
        List<String> a = new ArrayList<>();
        list.stream()
                .flatMap(order -> order.getMyItems().stream())
                .flatMap(item -> item.getIngredientsList().stream())
                .map(Ingredients::getName)
                .reduce("",(o1, o2) ->{
                    a.add(o1);
                    a.add(o2);
                    o2 = o1 + " " + o2;
                 return o2;
                } ).split(" ");
        return a;
    }
    private ArrayList<String> listWithOneElement(String a){
        List<String> list = new ArrayList<>();
        list.add(a);
        return (ArrayList<String>) list;
    }
    private ArrayList<String> collectList(List<String>a,List<String>b){
        List<String> list = new ArrayList<>();
        list.addAll(a);
        list.addAll(b);
        return (ArrayList<String>) list;
    }

    private BigDecimal getPice(List<Order> list){
//        Optional<Double> sumAll = list.stream()
//                .flatMap(order -> order.getMyItems().stream())
//                .map(Item::getPrice)
//                .map(bigDecimal -> bigDecimal.doubleValue())
//                .reduce((a,b)->a+b);


        BigDecimal sum = list.stream()
                .flatMap(order -> order.getMyItems().stream())
                .map(Item::getPrice)
                .reduce(BigDecimal.ZERO, (o1,o2)->o1.add(o2));

        return sum;
    }

    public static void main(String[] args) {
        StreamExample streamExample = new StreamExample();
        List<Order> orderList = new ArrayList<>();
        orderList.add(streamExample.createOrder1());
        orderList.add(streamExample.createOrder2());
        List<String> myIngredient = streamExample.getIngredient(orderList);
        System.out.println(myIngredient);
        System.out.println(streamExample.getPice(orderList));
        streamExample.getIngredientByReduceClassesVersion(orderList);





    }
}
