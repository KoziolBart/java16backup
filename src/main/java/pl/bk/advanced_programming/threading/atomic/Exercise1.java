package pl.bk.advanced_programming.threading.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Exercise1 {


    public static void main(String[] args) {
        AtomicInteger increment = new AtomicInteger();

        Runnable job = () -> {
           for(int i =0; i <100;i++){
            increment.incrementAndGet();
           }
            System.out.println(Thread.currentThread().getName());
        };

        ExecutorService workers = Executors.newFixedThreadPool(3);
        workers.execute(job);
        workers.execute(job);
        workers.execute(job);

        workers.shutdown();


        try {
            boolean test = true;
            do {

                test = workers.awaitTermination(1, TimeUnit.SECONDS);
                System.out.println(Thread.currentThread().getName());
            } while (!test);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

            System.out.println(increment);
    }
}
