package pl.bk.advanced_programming.threading.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class Exercise3 {
    public static void main(String[] args) {
        int [] arrayTest = {123,424,43,1435,253,525,25352,55,347,58,583465,4884,57453,73525,73466,4523};

        System.out.println(sumArray(arrayTest,5));
        System.out.println("testuje: " + sumTest(arrayTest));


    }

    public static long sumArray (int[] array, int threadPool){
        int [] tab = new int [threadPool];

        ExecutorService workers = Executors.newFixedThreadPool(threadPool);

        for (int i = 0; i < threadPool; i++){
            int finalI = i;
            workers.execute(() -> tab[finalI] = sumHalfArray(array, finalI,threadPool));
        }

        workers.shutdown();

        wait(workers);

        return sumTest(tab);
        }
    public static int sumHalfArray (int[] array, int numberHalf, int parameter){
        int result = 0;
        for(int i = numberHalf; i < array.length; i=i+parameter){
            result += array[i];
        }
        return result;
    }
    public static int sumTest (int[] array){
        int result = 0;
        for(int i = 0; i < array.length; i++){
            result += array[i];
        }
        return result;
    }
    public static void wait (ExecutorService workers){
        boolean done = false;
        do{
            try {
                done = workers.awaitTermination(200,TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while(!done);
    }

}





