package pl.bk.advanced_programming.threading.atomic;

public class Exercise5 {
    public static void main(String[] args) {
        Person kolejny = new Person("imie", "nawisko", null);
        try {
            synchronized (kolejny){
            kolejny.wait(10000);}
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(123);
    }
}
