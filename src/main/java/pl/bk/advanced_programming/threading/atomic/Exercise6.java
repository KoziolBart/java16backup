package pl.bk.advanced_programming.threading.atomic;



public class Exercise6 {

    public static void main(String[] args) {
        System.out.println("Początek testu...");
        Exercise6 exercise6 = new Exercise6();
        exercise6.createClock(20);

    }

    private void createClock (int size){
        TicOrTac tic = new TicOrTac("TIC");
        TicOrTac tac = new TicOrTac("TAC");


        loopMethod(tic,tac, size);

    }

    private void sleepper (TicOrTac TT){
        try {
            synchronized (TT){
            TT.wait();}
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void wakeUpAndSleep (TicOrTac TT){
        TT.giveName();
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TT.notifyAll();
        sleepper(TT);

    }

    private void loopMethod (TicOrTac First, TicOrTac Second ,int size) {
        sleepper(Second);
        for(int i = 0; i<size; i++){

            wakeUpAndSleep(First);
            wakeUpAndSleep(Second);
        }
    }

}

class TicOrTac implements Runnable {
    String name;
    Object synObject;

    protected void giveName() {
        System.out.println(name);
    }

    public TicOrTac(String name) {
        this.name = name;
    }

    @Override
    public void run() {


    }
}
